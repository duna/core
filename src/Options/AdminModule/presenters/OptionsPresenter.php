<?php

namespace Duna\Core\Options\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;
use App\Components\Flash\Flash;
use Duna\Core\Options\Components\IAclFactory;
use Duna\Core\Options\Components\IGeneralFactory;
use Duna\Core\Options\Components\IMenuFactory;
use Duna\Core\Options\Components\ISeoFactory;
use Duna\Extension\Manager\Registrator;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Core\Options\AdminModule\Presenters
 */
class OptionsPresenter extends BasePresenter
{

    /** @var \Duna\Extension\Manager\Registrator */
    private $registrator;

    public function __construct(Registrator $registrator)
    {
        parent::__construct();
        $this->registrator = $registrator;
    }

    public function renderDefault()
    {
        $this->template->extensions = $this->registrator->getExtensions();
    }

    /**
     * @secured
     */
    public function handleInstallExtension($md5)
    {
        $this->registrator->installExtension($md5);
        $this->flashMessage('Rozšíření bylo úspěšně nainstalováno.', Flash::SUCCESS);
        $this->redirect('this');
    }

    /**
     * @secured
     */
    public function handleUnInstallExtension($id)
    {
        $this->registrator->uninstallExtension($id);
        $this->flashMessage('Rozšíření bylo úspěšně odinstalováno.', Flash::SUCCESS);
        $this->redirect('this');
    }

    protected function createComponentOptionsMenu(IMenuFactory $factory)
    {
        return $factory->create();
    }

    protected function createComponentAcl(IAclFactory $factory)
    {
        return $factory->create();
    }

    protected function createComponentGeneral(IGeneralFactory $factory)
    {
        return $factory->create();
    }

    protected function createComponentSeo(ISeoFactory $factory)
    {
        return $factory->create();
    }

}
