<?php

namespace Duna\Core\Options\Components\Menu;

use Kdyby\Doctrine\EntityManager;
use Nette;

/**
 * @package Duna\Core\Options\Components\Menu
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class Component extends Nette\Application\UI\Control
{

    /** @var EntityManager */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function render()
    {
        $template = $this->template;

        $template->render(__DIR__ . '/default.latte');
    }

}
