<?php

namespace Duna\Core\Options\Components\General;

use App\Components\Flash\Flash;
use Duna\Core\Options\OptionFacade;
use Kdyby\Doctrine\EntityManager;
use Nette;

/**
 * @package Duna\Core\Options\Components\General
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class Component extends Nette\Application\UI\Control
{

    /** @var EntityManager */
    private $em;
    private $facade;

    public function __construct(EntityManager $em, OptionFacade $facade)
    {
        parent::__construct();
        $this->em = $em;
        $this->facade = $facade;
    }

    public function render()
    {
        $template = $this->template;
        $template->render(__DIR__ . '/default.latte');
    }

    public function formSuccess(Nette\Application\UI\Form $_, Nette\Utils\ArrayHash $values)
    {
        $this->facade->setOptions($values);
        $this->presenter->flashMessage('Nastavení bylo úspěšně změněno.', Flash::SUCCESS);
        $this->redirect('this');
    }

    protected function createComponentGeneral()
    {
        $form = new \Nette\Application\UI\Form;
        $form->addProtection();
        $form->addText($name = 'site_title', 'Název webu')
            ->setRequired('Prosím vyplňte název webu')
            ->setDefaultValue($this->facade->getOption($name));
        $form->addText($name = 'site_title_separator', 'Oddělovač nadpisů')
            ->setDefaultValue($this->facade->getOption($name));
        $form->addSubmit('save', 'Uložit');

        $form->onSuccess[] = [$this, 'formSuccess'];
        return $form;
    }

}
