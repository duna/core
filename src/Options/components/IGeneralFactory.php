<?php

namespace Duna\Core\Options\Components;

interface IGeneralFactory
{

    /** @return General\Component */
    function create();
}
