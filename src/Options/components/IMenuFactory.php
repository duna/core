<?php

namespace Duna\Core\Options\Components;

interface IMenuFactory
{

    /** @return Menu\Component */
    function create();
}
