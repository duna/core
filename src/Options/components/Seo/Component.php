<?php

namespace Duna\Core\Options\Components\Seo;

use App\Components\Flash\Flash;
use Duna\Core\Options\OptionFacade;
use Kdyby\Doctrine\EntityManager;
use Nette;

/**
 * Class Component
 *
 * @package Duna\Core\Options\Components\Seo
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class Component extends Nette\Application\UI\Control
{

    /** @var EntityManager */
    private $em;
    private $facade;

    public function __construct(EntityManager $em, OptionFacade $facade)
    {
        parent::__construct();
        $this->em = $em;
        $this->facade = $facade;
    }

    public function render()
    {
        $template = $this->template;
        $template->render(__DIR__ . '/default.latte');
    }

    public function formSuccess(Nette\Application\UI\Form $_, Nette\Utils\ArrayHash $values)
    {
        $this->facade->setOptions($values);
        $this->presenter->flashMessage('Nastavení bylo úspěšně změněno.', Flash::SUCCESS);
        $this->redirect('this');
    }

    protected function createComponentSeoForm()
    {
        $form = new \Nette\Application\UI\Form;
        $form->addProtection();

        $form->addSelect($name = 'page_url_end', 'Koncovka URL stránek', $ext = [
            null => 'Žádná', '/' => '/', '.htm' => '.htm', '.html' => '.html',
        ])->setDefaultValue($this->facade->getOption($name));
        $form->addSelect($name = 'page_category_end', 'Koncovka kategorii', $ext)
            ->setDefaultValue($this->facade->getOption($name));
        $form->addText($name = 'site_title_separator', 'Oddělovač v titulu')
            ->setDefaultValue($this->facade->getOption($name));
        $form->addSubmit('send', 'Uložit');

        $form->onSuccess[] = [$this, 'formSuccess'];
        return $form;
    }

}
