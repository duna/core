<?php

namespace Duna\Core\Options\Components;

interface ISeoFactory
{

    /** @return Seo\Component */
    function create();
}
