<?php

namespace Duna\Core\Options\Components;

interface IAclFactory
{

	/** @return Acl\Component */
	function create();
}
