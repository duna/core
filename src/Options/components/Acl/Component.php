<?php

namespace Duna\Core\Options\Components\Acl;

use Duna\Security\Authorizator;
use Duna\Security\Entity\Permission;
use Kdyby\Doctrine\EntityManager;
use Nette;

/**
 * @package Duna\Core\Options\Components\Acl
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class Component extends Nette\Application\UI\Control
{

    /** @var EntityManager */
    private $em;

    /** @var Nette\Caching\Cache */
    private $cache;

    /** @var Permission[] */
    private $permissions = [];

    public function __construct(EntityManager $em, Nette\Caching\IStorage $cache)
    {
        parent::__construct();
        $this->em = $em;
        $this->cache = new \Nette\Caching\Cache($cache, Authorizator::CACHE_NAMESPACE);

        $qb = $this->em->getRepository(Permission::class)->createQueryBuilder('p', 'p.id');
        $qb->leftJoin('p.role', 'ro')->addSelect('ro');
        $qb->leftJoin('p.resource', 're')->addSelect('re');
        $qb->orderBy('p.role DESC, re.name');

        $temp = $qb->getQuery()->getResult();
        foreach ($temp as $permission) {
            $this->permissions[$permission->getId()] = $permission;
        }
    }

    public function render()
    {
        $template = $this->template;
        $template->permissions = $this->permissions;
        $template->render(__DIR__ . '/default.latte');
    }

    public function formSuccess(Nette\Application\UI\Form $form)
    {
        $this->cache->clean([
            Nette\Caching\Cache::TAGS => [Authorizator::CACHE_NAMESPACE . '/permissions'],
        ]);
        $values = $form->getValues();
        if (array_key_exists($values->id, $this->permissions)) {
            $entity = $this->permissions[$values->id];
            $entity->setCreate($values->create);
            $entity->setRead($values->read);
            $entity->setUpdate($values->update);
            $entity->setDelete($values->delete);
            $this->em->persist($entity);
            $this->em->flush($entity);
        }
    }

    protected function createComponentAclForm()
    {
        $that = $this;
        return new \Nette\Application\UI\Multiplier(function ($id) use ($that) {
            $form = new \Nette\Application\UI\Form;
            $form->addCheckbox('create', null)->setDefaultValue($that->permissions[$id]->getCreate());
            $form->addCheckbox('read', null)->setDefaultValue($that->permissions[$id]->getRead());
            $form->addCheckbox('update', null)->setDefaultValue($that->permissions[$id]->getUpdate());
            $form->addCheckbox('delete', null)->setDefaultValue($that->permissions[$id]->getDelete());
            $form->addHidden('id', $id);
            $form->addSubmit('save', 'Uložit');
            $form->onSuccess[] = [$that, 'formSuccess'];
            return $form;
        });
    }

}
