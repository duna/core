<?php

namespace Duna\Core\Options;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Kdyby\Doctrine\EntityManager;
use Nette;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Core\Options
 */
class OptionFacade
{

    const CACHE_NAMESPACE = 'System.Option';
    /** @var EntityManager */
    private $em;
    /** @var \Nette\Caching\Cache */
    private $cache = null;

    public function __construct(EntityManagerInterface $em, Nette\Caching\IStorage $cache = null)
    {
        $this->em = $em;
        if ($cache !== null)
            $this->cache = new Nette\Caching\Cache($cache, self::CACHE_NAMESPACE);
    }

    public function delete($id, $autoFlush = false)
    {
        throw new Nette\NotImplementedException;
    }

    public function getById($id, $throwExceptions = true)
    {
        throw new Nette\NotImplementedException;
    }

    public function getByKey($name, $throwException = false)
    {
        $entity = $this->em->getRepository(Entity\Option::class)->findOneBy([
            'key eq' => $name,
        ]);

        if ($entity === null && $throwException)
            throw new Nette\InvalidStateException();

        return $entity;
    }

    public function getOption($name)
    {
        if ($this->cache)
            return $this->loadFormCache($name);
        else
            return $this->getByMame($name);
    }

    /**
     * @param array $data
     * @param boolean $throwExceptions
     * @return Entity\Option|null
     */
    public function insert(array $data, $throwExceptions = true)
    {
        $entity = new Entity\Option;
        $entity->setKey($data['key']);
        $entity->setValue($data['value']);

        $this->em->persist($entity);
        try {
            $this->em->flush($entity);
            return $entity;
        } catch (UniqueConstraintViolationException $e) {
            if ($throwExceptions)
                throw new Nette\InvalidStateException();
        }
    }

    public function setOptions(Nette\Utils\ArrayHash $values)
    {
        foreach ($values as $key => $value) {
            if ($value instanceof Nette\Utils\ArrayHash) {
                $this->setOptions($value);
            } else {
                $this->setOption($key, $value);
            }
        }
        $this->em->flush();
    }

    public function update($id, array $data, $throwExceptions = true, $autoFlush = false)
    {

    }

    private function isSerialized($value)
    {
        return ($value == serialize(false) || @unserialize($value) !== false);
    }

    private function loadFormCache($name)
    {
        $that = $this;
        $option = $this->cache->load($name, function (&$dep) use ($name, $that) {
            $option = $that->getByKey($name);
            if (!$option)
                return null;

            $dep = [\Nette\Caching\Cache::TAGS => [$name]];
            $value = $option->getValue();
            return ($this->isSerialized($value) ? unserialize($value) : $value);
        });
        return $option;
    }

    private function setOption($key, $value)
    {
        $optionValue = is_array($value) ? serialize($value) : $value;
        $entity = $this->getByKey($key);

        if (!$entity) {
            $entity = $this->insert([
                'key'   => $key,
                'value' => $optionValue,
            ], true);
        } else {
            $entity->setValue($optionValue);
            $this->em->persist($entity);
            $this->em->flush($entity);
        }
        if ($this->cache)
            $this->cache->clean([\Nette\Caching\Cache::TAGS => [$key]]);
        return $entity;
    }

}
