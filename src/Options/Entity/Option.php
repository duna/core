<?php

namespace Duna\Core\Options\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="system_option")
 * @ORM\Entity
 * 
 * @method string getValue()
 */
class Option
{

	use \Kdyby\Doctrine\Entities\MagicAccessors;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="system_option_id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="opt", type="string", length=32, nullable=false)
	 */
	protected $key;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="val", type="string", length=128, nullable=false)
	 */
	protected $value;

	public function getId()
	{
		return $this->id;
	}

	public function setValue($value)
	{
		$this->value = is_array($value) ? serialize($value) : $value;
		return $this;
	}

}
