<?php

namespace Duna\Core\DI;

use Composer\Autoload\ClassLoader;
use Duna\Console\IEntityConsoleProvider;
use Duna\Core\Cli\InstallCommand;
use Duna\DI\Extensions\CompilerExtension;
use Kdyby\Doctrine\DI\IEntityProvider;
use Nette\PhpGenerator\ClassType;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class Extension extends CompilerExtension implements IEntityProvider, IEntityConsoleProvider
{
    private $defaults = [
        'https' => true,
    ];

    private $loader;

    private $commands = [
        InstallCommand::class,
    ];

    public function __construct(ClassLoader $robotLoader)
    {
        $this->loader = $robotLoader;
    }

    public function afterCompile(ClassType $class)
    {
        $initialize = $class->getMethod('initialize');
        $config = $this->getConfig($this->defaults);
        if ($config['https']) {
            $initialize->addBody('\Nette\Application\Routers\Route::$defaultFlags = \Nette\Application\Routers\Route::SECURED;');
        }
    }

    public function beforeCompile()
    {
        $builder = $this->getContainerBuilder();
        $this->addPresenterMapping([
            "Dashboard"  => "Duna\\Core\\Dashboard\\*Module\\Presenters\\*Presenter",
            "Navigation" => 'Duna\\Core\\Navigation\\*Module\\Presenters\\*Presenter',
            "Options"    => 'Duna\\Core\\Options\\*Module\\Presenters\\*Presenter',
        ]);

        $definition = $builder->getDefinition('latte.latteFactory');
        $definition->addSetup('?->onCompile[] = function($engine) { Duna\Core\Navigation\Latte\Menu::install($engine->getCompiler()); }', ['@self']);

    }

    function getEntityMappings()
    {
        return self::entityMappings();
    }

    public function loadConfiguration()
    {
        $this->parseConfig(__DIR__ . '/config.neon');
        $this->addCommands($this->commands);
    }

    public static function entityMappings()
    {
        return [
            "Duna\\Core\\Navigation\\Entity" => __DIR__ . '/../Navigation/Entity',
            "Duna\\Core\\Options\\Entity"    => __DIR__ . '/../Options/Entity',
        ];
    }

}
