<?php

namespace Duna\Core\Navigation\Latte;

use Latte\Macros\MacroSet;
use Latte\Compiler;
use Latte\MacroNode;
use Latte\PhpWriter;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Core\Navigation\Latte
 */
class Menu extends MacroSet
{

	public static function install(Compiler $compiler)
	{
		$macroset = new static($compiler);
		$macroset->addMacro('menu', [$macroset, 'macroMenu']);
		return $macroset;
	}

	public function macroMenu(MacroNode $node, PhpWriter $writer)
	{
		$words = $node->tokenizer->fetchWords();
		if (!$words) {
			throw new \Latte\CompileException('Missing menu name in {menu}');
		}

		$name = $writer->formatWord($words[0]);
		return '$_l->tmp = $_contorl->getComponent(\'customMenu\'); '
				. '$_l->tmp->setIdentifier(' . $name . '); '
				. 'if ($_l->tmp instanceof Nette\Application\UI\IRenderable) $_l->tmp->redrawControl(null, false); '
				. ($writer->write("ob_start(); \$_l->tmp->render(); echo %modify(ob_get_clean())"));
	}

}
