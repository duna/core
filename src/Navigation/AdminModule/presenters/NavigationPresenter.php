<?php

namespace Duna\Core\Navigation\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;
use Duna\Core\Navigation\Components\IEditorMenuFactory;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Core\Navigation\AdminModule\Presenters
 */
class NavigationPresenter extends BasePresenter
{
	protected function createComponentEditMenu($name, IEditorMenuFactory $factory){
	    return $factory->create();
    }
}
