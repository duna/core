<?php

namespace Duna\Core\Navigation\Facade;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManagerInterface;
use Duna\Core\Facade\AbstractFacade;
use Duna\Core\Navigation\Entity;
use Duna\Router\Entity\Url;
use Nette\InvalidStateException;

/**
 * @package Duna\Core\Navigation
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class NavigationFacade extends AbstractFacade
{
    /**
     * {@inheritDoc}
     */
    public function getClassName()
    {
        return Entity\NavigationItem::class;
    }

    public function calculatePaths($root, array $nodeIds)
    {
        $order = 0;
        foreach ($nodeIds as $key => $id) {
            if (!is_array($id)) {
                $this->createPath($root, $id, $order);
            } else {
                $this->createPath($root, $key, $order);
                $this->calculatePaths($key, $id);
            }
            $order++;
        }
    }

    public function createItem($name, Entity\Navigation $navigation, Url $url = null, $root = false, $icon = null, $externalUrl = null, $parentId = null, $throwException = false)
    {
        $entity = new Entity\NavigationItem();
        $entity->name = $name;
        if ($url !== null)
            $entity->setUrl($url);
        if ($icon)
            $entity->setIcon($icon);
        if ($root)
            $entity->setRoot();
        if ($externalUrl)
            $entity->setExternalUrl($externalUrl);
        $this->em->persist($entity);

        try {
            $this->em->flush($entity);
            $this->createItemTree($entity, $navigation, $parentId);
            return $entity;
        } catch (UniqueConstraintViolationException $e) {
            if ($throwException)
                throw new InvalidStateException();
        }

    }

    public function createItemTree(Entity\NavigationItem $item, Entity\Navigation $nav, $parentId = null)
    {
        return $this->em->transactional(function () use ($item, $nav, $parentId) {
            $item->addNavigation($nav);
            $this->em->persist($item);
            $this->em->flush($item);

            $leaf = new Entity\NavigationTreePath;
            $leaf->setAncestor($item)->setDescendant($item);
            $this->em->persist($leaf);
            $this->em->flush($leaf);

            $root = $this->findRoot($nav->getId());
            if (!$root) {
                $root = (new Entity\NavigationItem)->setRoot()->setName(md5($nav->getName()));
                $root->addNavigation($nav);
                $rootPath = new Entity\NavigationTreePath;
                $rootPath->setAncestor($root)->setDescendant($root);
                $this->em->persist($rootPath);
                $this->em->flush($rootPath);
            }
            if ($parentId === null)
                $parentId = $root->getId();

            $this->createPath($parentId, $item->getId());
            return $item;
        });
    }

    /**
     * @param string $name
     * @param string $identifier
     * @param bool $throwException
     * @return \Duna\Core\Navigation\Entity\Navigation
     */
    public function createNavigation($name, $identifier, $throwException = false)
    {
        $entity = new Entity\Navigation();
        $entity->setIdentifier($identifier);
        $entity->name = $name;

        $this->em->persist($entity);
        try {
            $this->em->flush($entity);
            return $entity;
        } catch (UniqueConstraintViolationException $e) {
            if ($throwException)
                throw  new InvalidStateException();
        }
    }

    public function createPath($parentId, $itemId, $order = 0)
    {
        $conn = $this->em->getConnection();
        $sql = <<<SQL
INSERT INTO navigation_tree_path (ancestor_id, descendant_id, depth, item_order)
SELECT ancestor_id, :descSelect, depth+1, :itemOrder FROM navigation_tree_path
WHERE descendant_id = :desc;
SQL;
        return $conn->executeUpdate($sql, ['descSelect' => $itemId, 'desc' => $parentId, 'itemOrder' => $order], ['descSelect' => Type::INTEGER, 'desc' => Type::INTEGER, 'itemOrder' => Type::INTEGER]);
    }

    public function findRoot($id)
    {
        return $this->em->getRepository(Entity\NavigationItem::class)->findOneBy([
            'root'           => true,
            'navigations.id' => $id,
        ]);
    }

    public function getItem($name, Entity\Navigation $navigation, Url $url = null, $throwException = false)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('nait')
            ->from(Entity\NavigationItem::class, 'nait')
            ->join('nait.url', 'url')
            ->join('nait.navigations', 'navi')
            ->where($qb->expr()->eq('navi.id', $navigation->id))
            ->andWhere(($url === null) ? $qb->expr()->isNull('nait.url') : $qb->expr()->eq('url.id', $url->id))
            ->setMaxResults(1);

        $result = $qb->getQuery()->getOneOrNullResult();

        if ($result === null && $throwException)
            throw new InvalidStateException;

        return $result;
    }

    public function getItemTree($id)
    {
        $query = $this->em->getRepository(Entity\NavigationItem::class)->createQuery('
				SELECT n, IDENTITY(tree2.ancestor), IDENTITY(tree1.descendant), url FROM Duna\Core\Navigation\Entity\NavigationItem n
				LEFT JOIN Duna\Core\Navigation\Entity\NavigationTreePath tree1 WITH (n.id = tree1.descendant)
				LEFT JOIN Duna\Core\Navigation\Entity\NavigationTreePath tree2 WITH (tree2.descendant = tree1.descendant AND tree2.depth = 1)
				LEFT JOIN n.url url
				WHERE tree1.ancestor = ?1 AND tree1.depth > 0
				ORDER BY tree1.itemOrder ASC
				');
        $query->setParameter(1, $id);
        $items = $query->getResult();
        $nodes = [];
        foreach ($items as $item) {
            $nodes[$item[2]]['entity'] = $item[0];
            $nodes[$item[2]]['ancestor'] = (int) $item[1];
        }

        $createTree = function (&$nodes) {
            foreach ($nodes as $id => $info) {
                if (array_key_exists($info['ancestor'], $nodes)) {
                    $nodes[$info['ancestor']]['descendants'][$id] = $info;
                }
            }
            foreach ($nodes as $id => $info) {
                if (array_key_exists($info['ancestor'], $nodes)) {
                    $nodes[$info['ancestor']]['descendants'][$id] = $info;
                    unset($nodes[$id]);
                }
            }
        };
        $createTree($nodes);
        return \Nette\Utils\ArrayHash::from($nodes);
    }

    public function getNavigationByIdentifier($identifier, $throwException = false)
    {
        $entity = $this->em->getRepository(Entity\Navigation::class)->findOneBy([
            'identifier' => $identifier,
        ]);

        if ($entity === null && $throwException)
            throw new InvalidStateException();

        return $entity;
    }

    public function recalculatePathsForNode($nodeId, array $nodeIds)
    {
        $conn = $this->em->getConnection();
        $sql = <<<SQL
DELETE t1 FROM navigation_tree_path t1
JOIN navigation_tree_path t2 USING (descendant_id) WHERE (t2.ancestor_id = :ancestor AND t1.depth > 0);
SQL;
        $conn->executeUpdate($sql, ['ancestor' => $nodeId], ['ancestor' => Type::INTEGER]);
        $this->calculatePaths($nodeId, $nodeIds);
    }
}
