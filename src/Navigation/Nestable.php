<?php

namespace Duna\Core\Navigation;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @see https://dbushell.com/Nestable/
 */
class Nestable
{

	public static function resolveJson($input)
	{
		if (is_string($input))
			$input = \Nette\Utils\Json::decode($input);

		$tree = [];
		foreach ($input as $item) {
			if (!property_exists($item, 'children')) {
				$tree[$item->id] = $item->id;
			} else {
				$temp = [];
				foreach ($item->children as $child) {
					$temp[$child->id] = (property_exists($child, 'children') ? self::resolveJson($child->children) : $child->id);
				}
				$tree[$item->id] = $temp;
			}
		}
		return array_filter($tree);
	}

}
