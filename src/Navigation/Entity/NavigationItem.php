<?php

namespace Duna\Core\Navigation\Entity;

use Doctrine\ORM\Mapping as ORM;
use Duna\Router\Entity\Url;

/**
 * @ORM\Table(name="navigation_item", indexes={@ORM\Index(name="url_id", columns={"url_id"})})
 * @ORM\Entity
 *
 * @method setIcon(string $icon)
 * @method setName(string $name)
 * @method string getName()
 * @method setUrl(Url $url)
 * @method Url getUrl()
 * @method setExternalUrl(string $url)
 * @method string getExternalUrl();
 */
class NavigationItem
{

    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     */
    protected $icon = null;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    protected $name;
    /**
     * @var boolean
     *
     * @ORM\Column(name="root", type="boolean", nullable=false)
     */
    protected $root = false;
    /**
     * @var string
     *
     * @ORM\Column(name="external_url", type="string", length=255, nullable=true)
     */
    protected $externalUrl = null;
    /**
     * @var Url
     *
     * @ORM\ManyToOne(targetEntity="Duna\Router\Entity\Url")
     * @ORM\JoinColumn(name="url_id", referencedColumnName="url_id", nullable=true, onDelete="cascade")
     */
    protected $url;
    /**
     *
     * @var Navigation[]|\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Navigation", inversedBy="items", cascade={"persist"})
     * @ORM\JoinTable(name="navigation_item_navigation",
     *        joinColumns={@ORM\JoinColumn(name="navigation_item_id", referencedColumnName="navigation_item_id", onDelete="cascade")},
     *        inverseJoinColumns={@ORM\JoinColumn(name="navigation_id", referencedColumnName="navigation_id", onDelete="cascade")}
     * )
     */
    protected $navigations;
    /**
     * @var integer
     *
     * @ORM\Column(name="navigation_item_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    public function __construct()
    {
        $this->navigations = new \Doctrine\Common\Collections\ArrayCollection;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setRoot()
    {
        $this->root = true;
        return $this;
    }

}
