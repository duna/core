<?php

namespace Duna\Core\Navigation\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="navigation")
 * @ORM\Entity
 *
 * @method setName(string $name)
 * @method string getName()
 * @method setIdentifier(string $identifier)
 */
class Navigation
{

    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, nullable=false)
     */
    protected $name;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, nullable=false)
     */
    protected $identifier;
    /**
     * @var NavigationItem[]|\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="NavigationItem", mappedBy="navigations", cascade={"persist"})
     */
    protected $items;
    /**
     * @var integer
     *
     * @ORM\Column(name="navigation_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection;
    }

    public function getId()
    {
        return $this->id;
    }

}
