<?php

namespace Duna\Core\Navigation\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="navigation_tree_path", indexes={@ORM\Index(name="ancestor_id", columns={"ancestor_id"}), @ORM\Index(name="descendant_id", columns={"descendant_id"})})
 * @ORM\Entity
 */
class NavigationTreePath
{

    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $depth = 0;
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $itemOrder = 0;
    /**
     * @var NavigationItem
     *
     * @ORM\ManyToOne(targetEntity="NavigationItem", cascade={"persist"})
     * @ORM\JoinColumn(name="ancestor_id", referencedColumnName="navigation_item_id", onDelete="cascade")
     */
    protected $ancestor;
    /**
     * @var NavigationItem
     *
     * @ORM\ManyToOne(targetEntity="NavigationItem", cascade={"persist"})
     * @ORM\JoinColumn(name="descendant_id", referencedColumnName="navigation_item_id", onDelete="cascade")
     */
    protected $descendant;
    /**
     * @var integer
     *
     * @ORM\Column(name="navigation_tree_path_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    public function getId()
    {
        return $this->id;
    }

}
