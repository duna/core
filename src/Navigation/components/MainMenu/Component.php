<?php

namespace Duna\Core\Navigation\Components\MainMenu;

use Duna\Core\Navigation\Entity\Navigation;
use Duna\Core\Navigation\Facade\NavigationFacade;
use Kdyby\Doctrine\EntityManager;
use Nette;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class Component extends Nette\Application\UI\Control
{

    /** @var EntityManager */
    private $em;

    /** @var NavigationFacade */
    private $facade;

    public function __construct(EntityManager $em, NavigationFacade $facade)
    {
        $this->em = $em;
        $this->facade = $facade;
    }

    public function render()
    {
        $navigation = $this->em->getRepository(Navigation::class)->findOneBy([
            'identifier' => 'front',
        ]);
        $rootItem = $this->facade->findRoot($navigation->getId());

        $items = $this->facade->getItemTree($rootItem->getId());
        \Duna\Core\Navigation\Components\AdminMenu\Component::activeTreeview($items, $this->presenter->getAction(true));

        $template = $this->template;
        $template->items = $items;
        $template->render(__DIR__ . '/default.latte');
    }

}
