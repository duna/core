<?php

namespace Duna\Core\Navigation\Components\AdminMenu;

use Duna\Core\Navigation\Entity\Navigation;
use Duna\Core\Navigation\Entity\NavigationItem;
use Duna\Core\Navigation\Facade\NavigationFacade;
use Kdyby\Doctrine\EntityManager;
use Nette;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class Component extends Nette\Application\UI\Control
{

    /** @var EntityManager */
    private $em;

    /** @var NavigationFacade */
    private $facade;

    public function __construct(EntityManager $em, NavigationFacade $facade)
    {
        parent::__construct();
        $this->em = $em;
        $this->facade = $facade;
    }

    public function render()
    {
        $navigation = $this->em->getRepository(Navigation::class)->findOneBy([
            'identifier' => 'admin',
        ]);
        $rootItem = $this->facade->findRoot($navigation->getId());

        $items = $this->facade->getItemTree($rootItem->getId());
        self::activeTreeview($items, $this->presenter->getAction(true));

        $template = $this->template;
        $template->items = $items;
        $template->render(__DIR__ . '/default.latte');
    }

    public static function activeTreeview(&$items, $destination)
    {
        foreach ($items as $item) {
            /** @var $entity NavigationItem */
            $entity = $item->entity;
            if ($entity->url !== null && $entity->url->getAbsoluteDestination() === $destination) {
                return $entity->url;
            } elseif (isset($item->descendants)) {
                $result = self::activeTreeview($item->descendants, $destination);
                if ($result !== null) {
                    $entity->url = $result;
                    return $result;
                }
            }
        }
        return null;
    }

}
