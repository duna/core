<?php

namespace Duna\Core\Navigation\Components;

interface IMainMenuFactory
{

	/** @return MainMenu\Component */
	function create();
}
