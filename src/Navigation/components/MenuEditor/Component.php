<?php

namespace Duna\Core\Navigation\Components\EditorMenu;

use App\Components\Flash\Flash;
use Duna\Core\Navigation\Entity\Navigation;
use Duna\Core\Navigation\Entity\NavigationItem;
use Duna\Core\Navigation\Facade\NavigationFacade;
use Duna\Core\Navigation\Nestable;
use Kdyby\Doctrine\EntityManager;
use Nette;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class Component extends Nette\Application\UI\Control
{

    /** @var EntityManager */
    private $em;

    /** @var NavigationFacade */
    private $facade;
    /**
     * @persistent
     * @var null|Navigation
     */
    public $navigation_id = null;
    /** @var NavigationItem */
    private $item_root;

    public function __construct(EntityManager $em, NavigationFacade $navigationFacade)
    {
        $this->em = $em;
        $this->facade = $navigationFacade;
    }

    public function attached($presenter)
    {
        parent::attached($presenter);
        if ($presenter instanceof Nette\Application\UI\Presenter) {
            if ($this->navigation_id === null) {
                $this->navigation_id = $this->em->getRepository(Navigation::class)->findOneBy([])->getId();
            }
            $this->item_root = $this->facade->findRoot($this->navigation_id);
        }
    }

    public function render(array $params = null)
    {
        if ($this->item_root) {
            $this->template->menuItems = $this->facade->getItemTree($this->item_root->getId());
        } else { //new empty navigation
            $this->template->menuItems = [];
        }
        $this->template->render(__DIR__ . '/default.latte');
    }

    public function createComponentNavigationCreate($name)
    {
        $form = new Nette\Application\UI\Form($this, $name);
        $form->addText('navigation', 'Název nové navigace')
            ->setRequired('Zadejte prosím název nové navigace');
        $form->addSubmit('create', 'Vytvořit navigaci');

        $that = $this;
        $form->onSuccess[] = function ($_, Nette\Utils\ArrayHash $values) use ($that) {
            $that->facade->createNavigation($values->navigation, Nette\Utils\Strings::webalize($values->navigation));
            $that->presenter->flashMessage('Nové menu bylo úspěšně vytvořeno.', Flash::SUCCESS);
            $that->redirect('this');
        };
        return $form;
    }

    public function createComponentNavigationSelect()
    {
        $form = new Nette\Application\UI\Form();
        $form->addSelect('navigation', 'Navigace',
            $this->em->getRepository(Navigation::class)->findPairs('name')
        )->setDefaultValue($this->navigation_id ? : null);
        $form->addSubmit('delete', 'Smazat zvolenou navigaci')->onClick[] = function (Nette\Forms\Controls\SubmitButton $submitButton) {
            $values = $submitButton->getForm()->getValues();
            $partialEntity = $this->em->getPartialReference(Navigation::class, $values->navigation);
            $this->em->remove($partialEntity);
            $this->em->flush($partialEntity);
            $this->presenter->flashMessage('Navigace byla úspěšně smazána.', Flash::SUCCESS);
            $this->redirect('this');
        };
        $form->addSubmit('load', 'Načíst')->onClick[] = function (Nette\Forms\Controls\SubmitButton $submitButton) {
            $values = $submitButton->getForm()->getValues();
            $this->redirect('this', ['navigation_id' => $values->navigation]);
        };
        return $form;
    }

    public function createComponentMenuItems()
    {
        $form = new Nette\Application\UI\Form();
        $items = [];
        /** @var Category $category */
        $form->addSelect('categories', 'Kategorie:', [null => 'Vyberte kategorii'] + $items);
        $form->addText('title', 'Alternativní název:');
        $form->addText('href', 'URL adresa')
            ->addCondition($form::FILLED)
            ->addRule($form::URL, 'Vyplňte prosím platnou URL adresu');
        $form['title']->addConditionOn($form['href'], $form::FILLED)
            ->setRequired('Vyplňte prosím alternativní název odkazu.');
        $that = $this;
        $form->onSuccess[] = function (Nette\Application\UI\Form $form, Nette\Utils\ArrayHash $values) use ($that) {
            if (!(bool)array_filter((array)$values)) { //form is empty
                $form->addError('Vyberte prosím co chcete do menu přidat.');
                return;
            }

            $that->facade->createItem($values->title, $this->em->getRepository(Navigation::class)->find($that->navigation_id));
            $this->redirect('this'); //If AJAX redraw menu editor
        };
        $form->addSubmit('addItem', 'Přidat do menu');
        return $form;
    }

    public function handleUpdateNavigation($json)
    {
        if (!$this->presenter->isAjax()) {
            $this->redirect('this');
        }
        $this->facade->recalculatePathsForNode($this->item_root->getId(), Nestable::resolveJson($json));
        $this->getPresenter()->redrawControl('adminMenu');
    }

    public function handleDeleteNavigationItem($id)
    {
        $partialEntity = $this->em->getPartialReference(NavigationItem::class, $id);
        $this->em->remove($partialEntity);
        $this->em->flush();
        $this->redirect('this');
    }

}
