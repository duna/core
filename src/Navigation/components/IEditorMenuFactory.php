<?php

namespace Duna\Core\Navigation\Components;


interface IEditorMenuFactory
{
    /**
     * @return EditorMenu\Component
     */
    function create();

}