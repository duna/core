<?php

namespace Duna\Core\Navigation\Components;

interface IAdminMenuFactory
{

	/** @return AdminMenu\Component */
	function create();
}
