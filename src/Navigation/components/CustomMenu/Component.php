<?php

namespace Duna\Core\Navigation\Components;

use Doctrine\Common\Collections\ArrayCollection;
use Duna\Core\Navigation\Entity\Navigation;
use Duna\Core\Navigation\Facade\NavigationFacade;
use Kdyby\Doctrine\Collections\ReadOnlyCollectionWrapper;
use Kdyby\Doctrine\EntityManager;
use Nette;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class Component extends Nette\Application\UI\Control
{

    /** @var EntityManager */
    private $em;

    /** @var NavigationFacade */
    private $facade;
    private $items;

    public function __construct(EntityManager $em, NavigationFacade $facade)
    {
        parent::__construct();
        $this->em = $em;
        $this->facade = $facade;
        $this->items = new ReadOnlyCollectionWrapper(new ArrayCollection([]));
    }

    public function render()
    {
        $template = $this->template;
        $template->render(__DIR__ . '/default.latte');
    }

    public function setIdentifier($identifier)
    {
        $navigation = $this->em->getRepository(Navigation::class)->findOneBy([
            'identifier' => $identifier,
        ]);
        $rootItem = $this->facade->findRoot($navigation->getId());
        $itemTree = $this->facade->getItemTree($rootItem->getId());
        if ($itemTree)
            $this->items = $itemTree;
    }

}
