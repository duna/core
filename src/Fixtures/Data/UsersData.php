<?php

namespace Duna\Core\Fixtures\Data;

use Doctrine\ORM\EntityManagerInterface;
use Duna\Console\IInsertData;
use Duna\Security\Facade\RoleFacade;

class UsersData implements IInsertData
{
    private $defaultResources = [
        [
            'name'     => 'Dashboard - Administrace',
            'resource' => 'Dashboard:Admin:Dashboard',
        ],
        [
            'name'     => 'Dashboard - Frontend',
            'resource' => 'Dashboard:Front:Dashboard',
        ],
        [
            'name'     => 'Navigace',
            'resource' => 'Navigation:Admin:Navigation',
        ],
        [
            'name'     => 'Nastavení',
            'resource' => 'Options:Admin:Options',
        ],
        [
            'name'     => 'Uživatelé',
            'resource' => 'Security:Admin:Users',
        ],
    ];

    public function insert(EntityManagerInterface $em)
    {
        $facadeResource = new \Duna\Security\Facade\ResourceFacade($em);
        foreach ($this->defaultResources as $resource) {
            if (!$facadeResource->getByResource($resource['resource']))
                $facadeResource->insert($resource['name'], $resource['resource']);
        }

        $facadeRole = new RoleFacade($em);
        $facadePermission = new \Duna\Security\Facade\PermissionFacade($em);
        foreach ($this->defaultResources as $resource) {
            foreach ($facadeRole->getAll() as $role) {
                if (!$facadePermission->getBy($role, $resource['resource']))
                    $facadePermission->insert(true, true, true, true, $role, $resource['resource']);
            }
        }
    }

}
