<?php

namespace Duna\Core\Fixtures\Data;

use Doctrine\ORM\EntityManagerInterface;
use Duna\Console\IInsertData;

class OptionsData implements IInsertData
{

    private $defaults = [
        'site_title'                => 'Simple CMS',
        'site_title_separator'      => '|',
        'page_url_end'              => '',
        'page_category_end'         => '',
        'upload_max_filesize'       => 5242880,
        'upload_allowed_extensions' => '{}',
    ];

    public function insert(EntityManagerInterface $em)
    {
        $source = \Nette\Utils\ArrayHash::from($this->defaults);
        $facade = new \Duna\Core\Options\OptionFacade($em);
        $facade->setOptions($source);
    }

}
