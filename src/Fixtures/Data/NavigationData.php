<?php

namespace Duna\Core\Fixtures\Data;

use Doctrine\ORM\EntityManagerInterface;
use Duna\Console\IInsertData;
use Duna\Core\Navigation\Facade\NavigationFacade;
use Duna\Router\Facade\UrlFacade;

class NavigationData implements IInsertData
{
    private $defaults = [
        'admin' => [
            'name'  => 'Administrace - Hlavní menu',
            'items' => [
                [
                    'name'      => 'Nástěnka',
                    'icon'      => 'fa-home',
                    'locale'    => 'cs',
                    'link'      => 'administrace',
                    'presenter' => 'Dashboard:Admin:Dashboard',
                    'action'    => 'default',
                ],
                [
                    'name'      => 'Nastavení',
                    'icon'      => 'fa-cogs',
                    'locale'    => 'cs',
                    'link'      => 'administrace/nastaveni',
                    'presenter' => 'Options:Admin:Options',
                    'action'    => 'default',
                ],
                [
                    'name'      => 'Uživatelé',
                    'icon'      => 'fa-users',
                    'locale'    => 'cs',
                    'link'      => 'administrace/uzivatele',
                    'presenter' => 'Security:Admin:Users',
                    'action'    => 'default',
                ],
            ],
        ],
        'front' => [
            'name'  => 'Front - Hlavní menu',
            'items' => [
                [
                    'name'      => 'Titulní strana',
                    'locale'    => 'cs',
                    'link'      => '',
                    'icon'      => '',
                    'presenter' => 'Dashboard:Front:Dashboard',
                    'action'    => 'default',
                ],
            ],
        ],
    ];
    private $defaultUrls = [
        [
            'locale'    => 'cs',
            'link'      => 'prihlaseni',
            'presenter' => 'Auth:Sign',
            'action'    => 'in',
        ],
        [
            'locale'    => 'cs',
            'link'      => 'odhlaseni',
            'presenter' => 'Auth:Sign',
            'action'    => 'out',
        ],
        [
            'locale'    => 'cs',
            'link'      => 'administrace/nastaveni/seo',
            'presenter' => 'Options:Admin:Options',
            'action'    => 'seo',
        ],
        [
            'locale'    => 'cs',
            'link'      => 'administrace/nastaveni/acl',
            'presenter' => 'Options:Admin:Options',
            'action'    => 'acl',
        ],
        [
            'locale'    => 'cs',
            'link'      => 'administrace/nastaveni/hlavni',
            'presenter' => 'Options:Admin:Options',
            'action'    => 'general',
        ],
        [
            'locale'    => 'cs',
            'link'      => 'administrace/navigace',
            'presenter' => 'Navigation:Admin:Navigation',
            'action'    => 'default',
        ],
        [
            'locale'    => 'cs',
            'link'      => 'administrace/novy-uzivatel',
            'presenter' => 'Security:Admin:Users',
            'action'    => 'new',
        ],
        [
            'locale'    => 'cs',
            'link'      => 'administrace/uprava-uzivatele',
            'presenter' => 'Security:Admin:Users',
            'action'    => 'edit',
        ],
    ];

    public function insert(EntityManagerInterface $em)
    {
        $facadeUrl = new UrlFacade($em);
        foreach ($this->defaultUrls as $urlData) {
            if (!$facadeUrl->getBy($urlData['link'], $urlData['locale']))
                $facadeUrl->insert($urlData['link'], $urlData['presenter'], $urlData['locale'], $urlData['action']);
        }

        $facadeNavigation = new NavigationFacade($em);
        foreach ($this->defaults as $key => $data) {
            if (($nav = $facadeNavigation->getNavigationByIdentifier($key)) === null)
                $nav = $facadeNavigation->createNavigation($data['name'], $key);
            foreach ($data['items'] as $item) {
                if (($link = $facadeUrl->getBy($item['link'], $item['locale'])) === null)
                    $link = $facadeUrl->insert($item['link'], $item['presenter'], $item['locale'], $item['action']);
                if ($facadeNavigation->getItem($item['name'], $nav, $link) === null)
                    $facadeNavigation->createItem($item['name'], $nav, $link, false, $item['icon']);
            }
        }
    }
}