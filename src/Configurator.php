<?php

namespace Duna\Core;

use Composer\Autoload\ClassLoader;
use Duna;
use Kdyby;
use Nette\DI\Compiler;

class Configurator extends \Nette\Configurator
{
    /** @var \Composer\Autoload\ClassLoader */
    private $loader;

    private $extensions = [
        'console'         => Kdyby\Console\DI\ConsoleExtension::class,
        'events'          => Kdyby\Events\DI\EventsExtension::class,
        'annotations'     => Kdyby\Annotations\DI\AnnotationsExtension::class,
        'doctrine'        => Kdyby\Doctrine\DI\OrmExtension::class,
        'duna.console'    => Duna\Console\DI\Extension::class,
        'duna.plugins'    => Duna\Plugin\Manager\DI\Extension::class,
        'duna.extensions' => Duna\Extension\Manager\DI\Extension::class,
    ];

    public function __construct(ClassLoader $loader)
    {
        parent::__construct();
        $this->loader = $loader;

        foreach ($this->extensions as $key => $class) {
            $this->defaultExtensions[$key] = $class;
        }
    }

    public function createContainer()
    {
        $loader = $this->loader;
        $this->onCompile[] = function ($_, Compiler $compiler) use ($loader) {
            $compiler->addExtension('core', new DI\Extension($loader));
        };
        $this->onCompile[] = function ($_, Compiler $compiler) use ($loader) {
            Duna\Plugin\Manager\DI\Extension::load($compiler, $loader);
        };
        $this->onCompile[] = function ($_, Compiler $compiler) use ($loader) {
            Duna\Extension\Manager\DI\Extension::load($compiler, $loader);
        };

        return parent::createContainer();
    }


}