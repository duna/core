<?php

namespace Duna\Core\Dashboard\FrontModule\Presenters;

use App\FrontModule\Presenters\BasePresenter;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Core\Dashboard\FrontModule\Presenters
 */
class DashboardPresenter extends BasePresenter
{

}
