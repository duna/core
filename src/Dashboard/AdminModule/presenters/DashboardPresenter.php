<?php

namespace Duna\Core\Dashboard\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Core\Dashboard\AdminModule\Presenters
 */
class DashboardPresenter extends BasePresenter
{
	
}
