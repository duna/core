<?php

namespace Duna\Core\Facade;


use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Nette\InvalidStateException;

abstract class AbstractFacade implements IFacade
{
    /** @var \Doctrine\ORM\EntityManagerInterface */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritDoc}
     */
    public function create($entity, $throwException = false, $flush = true)
    {
        try {
            $this->em->persist($entity);
            $this->em->flush();
        } catch (UniqueConstraintViolationException $ex) {
            if ($throwException)
                throw new InvalidStateException($ex->getMessage());
            return null;
        }
        return $entity;
    }

    /**
     * {@inheritDoc}
     */
    public function update($entity, $throwException = false)
    {
        try {
            $this->em->persist($entity);
            $this->em->flush();
        } catch (UniqueConstraintViolationException $ex) {
            if ($throwException)
                throw new InvalidStateException($ex->getMessage());
            return null;
        }
        return $entity;
    }

    /**
     * {@inheritDoc}
     */
    public function delete($id, $throwException = false)
    {
        $entity = $this->getById($id, false);
        if ($entity === null && $throwException)
            throw new InvalidStateException();
        $this->em->remove($entity);
        $this->em->flush();

        return (int) $id;
    }

    /**
     * {@inheritDoc}
     */
    public function getById($id, $throwException = false)
    {
        $entity = $this->em->getRepository($this->getClassName())->find($id);

        if ($entity === null && $throwException)
            throw new InvalidStateException();

        return $entity;
    }

    /**
     * @return string
     */
    abstract public function getClassName();

    public function findPairs($value, $orderBy = [], $key = null)
    {
        if (!is_array($orderBy)) {
            $key = $orderBy;
            $orderBy = [];
        }

        if ($key === null) {
            $key = $this->em->getClassMetadata($this->getClassName())->getSingleIdentifierFieldName();
        }

        $qb = $this->em->createQueryBuilder();
        foreach ($orderBy as $by => $sort) {
            $qb->orderBy("e.$by", $sort);
        }
        $query = $qb->select("e.$value", "e.$key")
            ->from($this->getClassName(), 'e', 'e.' . $key)
            ->getQuery();

        try {
            return array_map(function ($row) {
                return reset($row);
            }, $query->getResult(AbstractQuery::HYDRATE_ARRAY));
        } catch (\Exception $e) {
            throw new InvalidStateException($query->getDQL());
        }
    }
}