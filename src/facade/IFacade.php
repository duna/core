<?php

namespace Duna\Core\Facade;


interface IFacade
{
    /**
     * Insert object to database
     *
     * @param object $entity
     * @param bool   $throwException
     * @return object Inserted object
     */
    public function create($entity, $throwException = false);

    /**
     * Update existing object
     *
     * @param object $entity
     * @param bool   $throwException
     * @return object Updated object
     */
    public function update($entity, $throwException = false);

    /**
     * Delete object by id
     *
     * @param integer $id
     * @param bool    $throwException
     * @return integer Id of deleted object
     */
    public function delete($id, $throwException = false);

    /**
     * Select obeject by id
     *
     * @param integer $id
     * @param bool    $throwException
     * @return object Selected object
     */
    public function getById($id, $throwException = false);
}