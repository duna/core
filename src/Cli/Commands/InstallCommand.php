<?php

namespace Duna\Core\Cli\Commands;

use Doctrine\ORM\Tools\ToolsException;
use Duna\Console\Command;
use Duna\Core\DI\Extension;
use Duna\Core\Fixtures\Data\NavigationData;
use Duna\Core\Fixtures\Data\OptionsData;
use Duna\Core\Fixtures\Data\UsersData;

class InstallCommand extends Command
{
    public function getData()
    {
        return [
            NavigationData::class,
            OptionsData::class,
            UsersData::class,
        ];
    }

    public function getDependentCommands()
    {
        return [
            ['command' => 'plugin:manager:install'],
            ['command' => 'extension:manager:install'],
            ['command' => 'plugin:localization:install'],
            ['command' => 'plugin:router:install'],
            ['command' => 'plugin:security:install'],
        ];
    }

    public function getEntityMappings($onlyKey = false, $data = [])
    {
        return parent::getEntityMappings($onlyKey, Extension::entityMappings());
    }

    public function getExtension()
    {
        return Extension::class;
    }

    public function getTitle()
    {
        return 'Duna Core';
    }

    public function runCommand()
    {
        $result = $this->getMetadataTables();
        try {
            $this->addMessageInfo('Creating database schema...');
            $this->schemaTool->updateSchema($result, true);
            return 0;
        } catch (ToolsException $e) {
            return 1;
        }
    }

    protected function configure()
    {
        $this->setName('duna:install')
            ->setDescription('Create database schema and load data fixtures.');
    }
}